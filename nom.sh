#!/bin/sh
generate() {
	[ ! -d ./blobcatnom ] && mkdir ./blobcatnom
	while IFS= read -r line; do
		food=${line%% *}
		name=${line#* }
		convert -background None lowerbody.png \
			\( -pointsize 25 -font "Noto-Emoji" pango:"$food" -gravity center -geometry -12.5+10 -fill None \) \
			-composite upperbody.png \
			-layers flatten \
			./blobcatnom/blobcatnom"$(echo "$name" | sed 's/ //g')".png
	done <food.txt
}

compress() {
	[ -f ./blobcatnom.zip ] && rm ./blobcatnom.zip
	if command -v zip; then
		zip -r ./blobcatnom.zip ./blobcatnom
	elif command -v 7z; then
		7z a ./blobcatnom.zip ./blobcatnom
	elif bsdtar -v 7z; then
		bsdtar -a -cf ./blobcatnom.zip ./blobcatnom
	fi
}

usage() {
	echo "$0 [gen|zip]"
}

case "$1" in
zip)
	compress
	;;
gen)
	generate
	;;
*)
	usage
	;;
esac
